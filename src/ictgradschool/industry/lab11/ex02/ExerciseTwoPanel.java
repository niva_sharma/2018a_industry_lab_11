package ictgradschool.industry.lab11.ex02;

import com.sun.org.apache.bcel.internal.generic.NEW;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.PublicKey;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 * <p>
 * TODO Complete this class. No hints this time :)
 */


public class ExerciseTwoPanel extends JPanel implements ActionListener {

    /**
     * Creates a new ExerciseFivePanel.
     */
    private JTextField textFieldvalue1;
    private JTextField textFieldvalue2;

    private JButton addButton;
    private JButton subtractButton;

    private JLabel resultLabel;

    private JTextField resultTextField;

    public ExerciseTwoPanel() {
        setBackground (Color.white);

        // Construct the TEXFILRDS, LABELS, BUTTONS

        textFieldvalue1 = new JTextField ();
        textFieldvalue2 = new JTextField ();

        addButton = new JButton ("Add");

        subtractButton = new JButton ("Subtract");

        addButton.setBounds (10, 10, 10, 10);
        subtractButton.setBounds (10, 10, 10, 10);
        addButton.addActionListener ((ActionListener) this);
        subtractButton.addActionListener ((ActionListener) this);

        resultTextField = new JTextField ();
        resultTextField.setColumns (10);

        resultLabel = new JLabel ();
        resultLabel.setText ("Result :");

        //Add JLabels, JTextFields and JButtons to window.


        this.add (textFieldvalue1);
        textFieldvalue1.setColumns (10);
        this.add (textFieldvalue2);
        textFieldvalue2.setColumns (10);

        this.add (addButton);
        this.add (subtractButton);

        this.add (resultLabel);
        this.add (resultTextField);


    }

    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        if (event.getSource () == addButton) {
            resultTextField.setText (String.valueOf (add (this.textFieldvalue1.getText (), this.textFieldvalue2.getText ())));
        } else if (event.getSource () == subtractButton) {
            resultTextField.setText (String.valueOf (subtract (this.textFieldvalue1.getText (), this.textFieldvalue2.getText ())));
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     *
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round (amount * 100)) / 100;
    }


    private double add(String value1, String value2) {
        double value = Double.parseDouble (value1);
        double value_2 = Double.parseDouble (value2);

        double sum = value + value_2;
        double roundedSum = roundTo2DecimalPlaces (sum);
        return roundedSum;
    }

    private double subtract(String value1, String value2) {
        double value = Double.parseDouble (value1);
        double value_2 = Double.parseDouble (value2);

        double sub = value - value_2;
        double roundedSub = roundTo2DecimalPlaces (sub);
        return roundedSub;
    }


}
