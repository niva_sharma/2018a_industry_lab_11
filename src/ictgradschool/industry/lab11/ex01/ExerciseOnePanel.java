package ictgradschool.industry.lab11.ex01;

import com.sun.rowset.internal.Row;
import javafx.beans.binding.DoubleExpression;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.PrivateKey;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JTextField textFieldHeight;
    private JTextField textFieldWeight;
    private JTextField textFieldBmi;
    private JTextField textFieldHealthyWeight;
    private JButton calculateHealthyWeightButton;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground (Color.white);


        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
//        GridLayout gridLayout = new GridLayout (3, 4);
//        this.setLayout (gridLayout);

        textFieldHeight = new JTextField (10);
        textFieldWeight = new JTextField (10);
        textFieldBmi = new JTextField (10);
        textFieldHealthyWeight = new JTextField (10);

        //BMI button
        calculateBMIButton = new JButton ();
        calculateBMIButton.setText ("Calculate BMI");
        calculateBMIButton.setBounds (10, 10, 10, 10);


        //Helthyeihjt button
        calculateHealthyWeightButton = new JButton ();
        calculateHealthyWeightButton.setText ("Calculate Healthy Weight");
        calculateHealthyWeightButton.setBounds (10, 10, 10, 10);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.

        JLabel heightLabel;
        JLabel weightLabel;
        JLabel bmiLabel;
        JLabel healthyWeightLabel;


        heightLabel = new JLabel ("Height in meters: ");

        weightLabel = new JLabel ("Weight in Kilograms: ");

        bmiLabel = new JLabel ("Your Body Mass Index(BMI) is ");

        healthyWeightLabel = new JLabel ("Maximum healthy weight for your Height: ");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)


        //1st row
        this.add (heightLabel);
        this.add (textFieldHeight);
        this.add (weightLabel);
        this.add (textFieldWeight);


        //2nd row
        this.add (calculateBMIButton);
        this.add (bmiLabel);
        this.add (textFieldBmi);

        //3rd row

        this.add (calculateHealthyWeightButton);
        this.add (healthyWeightLabel);
        this.add (textFieldHealthyWeight);


        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener (this);
        calculateHealthyWeightButton.addActionListener (this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.

        if (event.getSource () == calculateBMIButton) {
            textFieldBmi.setText (String.valueOf (calculateBmi (this.textFieldHeight.getText (), this.textFieldWeight.getText ())));
        } else if (event.getSource () == calculateHealthyWeightButton) {

            textFieldHealthyWeight.setText (String.valueOf (calculateHealthyWeight (this.textFieldHeight.getText (), this.textFieldWeight.getText ())));
        }


    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     *
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round (amount * 100)) / 100;
    }


    private double calculateBmi(String height, String weight) {
        Double height1 = Double.parseDouble (height);
        Double weight1 = Double.parseDouble (height);

        double bmi = (weight1) / (height1 * height1);
        double bmirounded = roundTo2DecimalPlaces (bmi);
        return bmirounded;
    }

    private double calculateHealthyWeight(String height, String weight) {
        Double height1 = Double.parseDouble (height);
        Double weight1 = Double.parseDouble (height);

        Double healthyWeight = 24.9 * (height1 * height1);
        Double roundedHealthyWeight = roundTo2DecimalPlaces (healthyWeight);
        return roundedHealthyWeight;
    }
}
