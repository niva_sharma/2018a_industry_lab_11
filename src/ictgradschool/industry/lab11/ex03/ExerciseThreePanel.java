package ictgradschool.industry.lab11.ex03;

import javax.swing.*;
import java.awt.*;
import java.awt.font.LineMetrics;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;


/**
 * A JPanel that draws some houses using a Graphics object.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseThreePanel extends JPanel {

    /** All outlines should be drawn this color. */
    private static final Color OUTLINE_COLOR = Color.black;

    /** The main "square" of the house should be drawn this color. */
    private static final Color MAIN_COLOR = new Color(255, 229, 204);

    /** The door should be drawn this color. */
    private static final Color DOOR_COLOR = new Color(150, 70, 20);

    /** The windows should be drawn this color. */
    private static final Color WINDOW_COLOR = new Color(255, 255, 153);

    /** The roof should be drawn this color. */
    private static final Color ROOF_COLOR = new Color(255, 153, 51);

    /** The chimney should be drawn this color. */
    private static final Color CHIMNEY_COLOR = new Color(153, 0, 0);

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseThreePanel() {
        setBackground(Color.white);
    }

    /**
     * Draws eight houses, using the method that you implement for this exercise.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawHouse(g, 125, 177, 3);
        drawHouse(g, 199, 193, 7);
        drawHouse(g, 292, 55, 5);
        drawHouse(g, 29, 110, 8);
        drawHouse(g, 379, 386, 7);
        drawHouse(g, 127, 350, 12);
        drawHouse(g, 289, 28, 2);
        drawHouse(g, 300, 150, 16);
    }

    /**
     * Draws a single house, with its top-left at the given coordinates, and with the given size multiplier.
     *
     * @param g the {@link Graphics} object to use for drawing
     * @param left the x coordinate of the house's left side
     * @param top the y coordinate of the top of the house's roof
     * @param size the size multipler. If 1, the house should be drawn as shown in the grid in the lab handout.
     */
    private void drawHouse(Graphics g, int left, int top, int size) {

        // TODO Draw a house, as shown in the lab handout.

        //triangle
        /*Polygon poly = new Polygon();
        poly.addPoint();
        poly.addPoint();
        poly.addPoint();*/
        //chim
//        g.fillRect (7,2,10, 5);
//        g.setColor (CHIMNEY_COLOR);
//        //main
//        g.fillRect (0,5,10,5);
//        g.setColor (MAIN_COLOR);
//        //window1
//        g.fillRect (2,4,3,3);
//        g.setColor (WINDOW_COLOR);
//
//        //window2
//        g.fillRect (8,10,3, 3);
//       g.setColor (WINDOW_COLOR);
//        //door
//        g.fillRect (5, 9, 4, 6);
//        g.setColor (DOOR_COLOR);

//        int halfMeasurement = size / 2;
//
//        Point2D.Double topRoof = new Point2D.Double(left, top - halfMeasurement);
//        Point2D.Double leftBottomRoof = new Point2D.Double(left - halfMeasurement, top);
//        Point2D.Double rightBottomRoof = new Point2D.Double(left + halfMeasurement, top);
//
//        // Roof lines.
//        Line2D.Double leftRoof = new Line2D.Double(topRoof, leftBottomRoof);
//        Line2D.Double rightRoof = new Line2D.Double(topRoof, rightBottomRoof);
//
//        //creates rooftop triangle
//
//
//
//        // Creates the house itself.
//        int xCoord = (int) leftBottomRoof.getX();
//        int yCoord = (int) leftBottomRoof.getY();
//
//        Rectangle house = new Rectangle(xCoord, yCoord, size, size);
//
//        // Creating the rectangle that represents the door of the house.
//        xCoord += size / 5;
//        yCoord += halfMeasurement;
//
//        Rectangle door = new Rectangle(xCoord + 2, yCoord,size / 4, halfMeasurement);
//
//        // Creating the rectangle that represents the window of the house.
//        xCoord += size / 2.5;
//
//        yCoord += size / 10;
//
//        Rectangle window1 = new Rectangle(xCoord, yCoord, size / 5, size / 4);
//        Rectangle window2 = new Rectangle(xCoord - 24, yCoord, size / 5, size / 4);
//
//        // Draws all the components.
//
//
//        g.draw(leftRoof);
//        g.draw(rightRoof);
//        g.draw(house);
//        g.draw(door);
//        g.draw(window1);
//        g.draw (window2);
//

        // create triangle
       // g.drawPolygon (new int[]{0,5}, new int[]{});
        Color fillColor;
        Color lineColor;
        int x;
        int y;
        int[] polyX;
        int[] polyY;
        int width;
        int height;


        // Chimney
        fillColor = CHIMNEY_COLOR;
        lineColor = Color.black;
        x = left + 7 * size;
        y = top + 1 * size;
        width = 1 * size;
        height = 2 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);



        // Roof
        fillColor = ROOF_COLOR;
        lineColor = Color.black;
        polyX = new int[] {
                left + 0 * size,
                left + 10 * size,
                left + 5 * size,
                left + 0 * size
        };
        polyY = new int[] {
                top + 5 * size,
                top + 5 * size,
                top + 0 * size,
                top + 5 * size
        };

        g.setColor (fillColor);
        g.fillPolygon (polyX, polyY, polyX.length);
        g.setColor (lineColor);
        g.drawPolygon (polyX, polyY, polyX.length);





        // House
        fillColor = MAIN_COLOR;
        lineColor = Color.black;
        x = left + 0 * size;
        y = top + 5 * size;
        width = 10 * size;
        height = 7 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);


        //window1

        fillColor = WINDOW_COLOR;
        lineColor = Color.black;
        x = left + 1 * size;
        y = top + 7 * size;
        width = 2 * size;
        height = 2 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);

       // window2

        lineColor = Color.black;
        x = left + 7 * size;
        y = top + 7 * size;
        width = 2 * size;
        height = 2 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);

        //Window cubes
        //cube_1
        fillColor = WINDOW_COLOR;
        lineColor = Color.black;
        x = left + 1 * size;
        y = top + 7 * size;
        width = 1 * size;
        height = 1 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);

        //cube_2
        fillColor = WINDOW_COLOR;
        lineColor = Color.black;
        x = left + 2 * size;
        y = top + 7 * size;
        width = 1 * size;
        height = 1 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);

        //cube_3
        fillColor = WINDOW_COLOR;
        lineColor = Color.black;
        x = left + 1 * size;
        y = top + 8 * size;
        width = 1 * size;
        height = 1 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);

        //cube_4
        lineColor = Color.black;
        x = left + 7 * size;
        y = top + 7 * size;
        width = 1 * size;
        height = 1 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);

        //cube_5
        lineColor = Color.black;
        x = left + 7 * size;
        y = top + 7 * size;
        width = 1 * size;
        height = 1 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);

        //cube_6

        lineColor = Color.black;
        x = left + 8 * size;
        y = top + 7 * size;
        width = 1 * size;
        height = 1 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);

        //last_cube:)
        lineColor = Color.black;
        x = left + 8 * size;
        y = top + 8 * size;
        width = 1 * size;
        height = 1 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);

        //door
        lineColor = Color.black;
        fillColor = DOOR_COLOR;
        x = left + 4 * size;
        y = top + 8 * size;
        width = 2 * size;
        height = 4 * size;

        g.setColor (fillColor);
        g.fillRect (x, y, width, height);
        g.setColor (lineColor);
        g.drawRect (x, y, width, height);
    }
}
